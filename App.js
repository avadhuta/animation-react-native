import React, {useEffect, useRef, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Animated,
  Easing,
  Image,
  TouchableOpacity,
} from 'react-native';
import SquareAnimation from './compoments/ani';
import MakeItRain from './compoments/moneyRich';
import * as Animatable from 'react-native-animatable';
import RotatingView from './compoments/roatate';
import LeftAnimationView from './compoments/leftAnimation';
import AnimationLeftView from './compoments/left';
import AudioPlayer from './compoments/audio';
import animationService from './service/animationService';

const IMG_URI = 'https://webstockreview.net/images/clipart-sun-round-5.png';

const App = () => {
  const translation = useRef(new Animated.ValueXY({x: 0, y: 0})).current;
  const [roationValues, setRotationValues] = useState(null);
  const [leftToRightValues, setLeftToRightValue] = useState(null);

  const findItemByName = (list, name) => {
    return list && list.length && list.find(item => item['ParamName'] == name);
  };

  const getAnimationParamsForRotation = async () => {
    try {
      const {data: rotate} =
        await animationService.getAnimationParamsForRotation();
      console.log(
        '🚀 ~ file: App.js ~ line 34 ~ getAnimationParamsForRotation ~ rotate',
        rotate,
      );
      if (rotate) {
        const rotateType = findItemByName(rotate, 'rotate_direction');
        const rotateDuration = findItemByName(rotate, 'rotate_duration');
        const data = {
          rotate: rotateType.ParamValue,
          duration: +rotateDuration.ParamValue,
        };
        setRotationValues(data);
      }
    } catch (err) {
      console.log(
        '🚀 ~ file: App.js ~ line 33 ~ getAnimationParamsForRotation ~ err',
        err,
      );
    }
  };
  const getAnimationParamsForLeftToRight = async () => {
    try {
      const {data: leftToRight} =
        await animationService.getAnimationParamsForLeftToRight();
      console.log(
        '🚀 ~ file: App.js ~ line 34 ~ getAnimationParamsForLeftToRight ~ leftToRight',
        leftToRight,
      );
      if (leftToRight) {
        const distance = findItemByName(leftToRight, 'distance');
        const duration = findItemByName(leftToRight, 'duration');
        console.log('🚀 distance', distance, duration);
        const data = {
          distance: +distance.ParamValue,
          duration: +duration.ParamValue,
        };
        setLeftToRightValue(data);
      }
    } catch (err) {
      console.log(
        '🚀 ~ file: App.js ~ line 33 ~ getAnimationParamsForLeftToRight ~ err',
        err,
      );
    }
  };

  useEffect(() => {
    getAnimationParamsForLeftToRight();
    getAnimationParamsForRotation();
  }, []);

  return (
    console.log(leftToRightValues, roationValues, 'leftToRightValues'),
    (
      <ScrollView>
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: '#fff',
            padding: 0,
          }}>
          <AudioPlayer />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 50,
            }}>
            {roationValues && (
              <RotatingView
                duration={roationValues.duration}
                rotateType={roationValues.rotate}
                onFinishedAnimating={status => {
                  console.log(status);
                }}
              />
            )}
          </View>
          {leftToRightValues && (
            <AnimationLeftView
              xDeg={0}
              yDeg={leftToRightValues.distance}
              duration={leftToRightValues.duration}
            />
          )}
          {leftToRightValues && (
            <AnimationLeftView
              xDeg={leftToRightValues.distance}
              yDeg={0}
              duration={leftToRightValues.duration}
            />
          )}
        </View>
      </ScrollView>
    )
  );
};

export default App;
