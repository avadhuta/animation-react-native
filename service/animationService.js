import http from './httpService';

const getAnimationParamsForLeftToRight = () => {
  return http.get('/pagecontent/getAnimationParms/2');
};
const getAnimationParamsForRotation = () => {
  return http.get('/pagecontent/getAnimationParms/1');
};

export default {
  getAnimationParamsForLeftToRight,
  getAnimationParamsForRotation,
};
