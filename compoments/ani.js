import React, {Component} from 'react';
import {View, Animated} from 'react-native';

class SquareAnimation extends Component {
  componentWillMount() {
    this.position = new Animated.ValueXY(0, 0);
    Animated.spring(this.position, {
      toValue: {x: 0, y: 100},
      duration: 5000,
    }).start();
    Animated.spring(this.position, {
      toValue: {x: 150, y: 300},
      duration: 4000,
    }).start();
    Animated.spring(this.position, {
      toValue: {x: 250, y: 500},
      duration: 6000,
    }).start();
  }

  render() {
    return (
      <Animated.View style={this.position.getLayout()}>
        <View style={styles.square} />
      </Animated.View>
    );
  }
}

const styles = {
  square: {
    width: 120,
    height: 120,
    backgroundColor: '#00BCD4',
  },
};
export default SquareAnimation;
