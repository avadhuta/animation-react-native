import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Easing,
  TouchableOpacity,
  Text,
  Dimensions,
  StatusBar,
  Image,
} from 'react-native';
const IMG_URI = 'https://webstockreview.net/images/clipart-sun-round-5.png';

class LeftAnimationView extends Component {
  constructor(props) {
    super(props);
    let screenWidth = Dimensions.get('window').width,
      screenHeight = Dimensions.get('window').height;
    this.state = {
      MainPosition: [
        styles.main,
        {width: screenWidth * 2},
        {marginTop: 0},
        {marginLeft: 0},
      ],
      paneDimensions: [
        styles.pane,
        {width: screenWidth},
        {height: screenHeight},
      ],
    };
  }

  SlidePane = direction => {
    let screenHeight = Dimensions.get('window').height,
      screenWidth = Dimensions.get('window').width,
      theLeftMargin;
    if (direction === 'right') {
      theLeftMargin = parseInt('-' + screenWidth);
      Animated.timing(this.animatedLeftMargin, {
        toValue: theLeftMargin,
        duration: 3000,
      }).start();
    }
    this.setState({
      MainPosition: [
        styles.main,
        {width: screenWidth * 2},
        {height: screenHeight},
        {marginTop: 0},
      ],
    });
  };
  componentDidMount() {
    this.animatedLeftMargin = new Animated.Value(0);
  }

  render() {
    return (
      <>
        <View style={{backgroundColor: 'red'}}>
          <TouchableOpacity onPress={() => this.SlidePane('right')}>
            <Text style={styles.buttonText}>Slide Right</Text>
          </TouchableOpacity>
        </View>
        <Animated.View
          style={[
            this.state.MainPosition,
            {marginLeft: this.animatedLeftMargin},
          ]}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Image
              style={{
                height: 100,
                width: 100,
                resizeMode: 'contain',
                borderRadius: 1000,
              }}
              resizeMode="contain"
              source={{uri: IMG_URI}}
            />
          </View>
        </Animated.View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'yellow',
  },
  row: {
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },
  pane: {
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 50,
    borderTopColor: 'transparent',
    backgroundColor: 'hsla(38, 100%, 73%, 1)',
  },
  paneText: {
    fontSize: 20,
    color: 'black',
  },
  buttonsContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    paddingTop: 0,
    paddingBottom: 3,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 3,
    padding: 10,
  },
  buttonText: {
    fontSize: 20,
    color: '#333',
    justifyContent: 'flex-end',
  },
});

export default LeftAnimationView;
