import React, {Component} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Animated,
  Easing,
  Image,
  TouchableOpacity,
} from 'react-native';
const IMG_URI = 'https://webstockreview.net/images/clipart-sun-round-5.png';

class RotatingView extends Component {
  state = {
    spinValue: new Animated.Value(0),
    startAnimation: false,
  };

  runAnimation = () => {
    this.state.spinValue.setValue(0);
    Animated.timing(this.state.spinValue, {
      toValue: this.props.toValue || 1,
      duration: this.props.duration || 2000,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => {
      if (this.state.startAnimation) {
        this.runAnimation();
      }
    });
  };

  stopAnimation = () => {
    console.log('calling stop animation');
    this.state.spinValue.setValue(0);
    Animated.timing(this.state.spinValue, {
      toValue: 0,
      duration: 0,
      easing: Easing.linear,
      useNativeDriver: true,
    }).stop();
  };

  componentDidMount() {
    // this.runAnimation();
  }

  handleRotateAnimation = () => {
    const currentState = !this.state.startAnimation;
    console.log(currentState, 'currentState');
    this.setState({startAnimation: !this.state.startAnimation});
    if (currentState) {
      this.runAnimation();
    }
    if (!currentState) {
      this.stopAnimation();
    }
  };

  render() {
    let spin = this.state.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [
        '0deg',
        this.props.rotateType == 'clockwise' ? '360deg' : '-360deg',
      ],
      extrapolate: 'clamp',
    });
    return (
      <Animated.View
        style={{
          ...this.props.style,
          transform: [{rotate: spin}],
        }}>
        <TouchableOpacity onPress={() => this.handleRotateAnimation()}>
          <Image
            style={{
              height: 200,
              width: 200,
              resizeMode: 'contain',
              borderRadius: 1000,
            }}
            resizeMode="contain"
            source={{uri: IMG_URI}}
          />
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

export default RotatingView;
