import React, {Component} from 'react';
import {StyleSheet, Text, Image, View, Animated, Easing} from 'react-native';
const IMG_URI = 'https://webstockreview.net/images/clipart-sun-round-5.png';
class AnimationLeftView extends Component {
  constructor() {
    super();
    this.animatedValue = new Animated.Value(0);
  }
  componentDidMount() {
    this.animate();
  } //animate method is call from componentDidMount
  animate() {
    this.animatedValue.setValue(0);
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: this.props.duration ? this.props.duration : 2000,
      easing: Easing.linear,
    }).start(() => this.animate());
  }

  render() {
    const marginLeft = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [this.props.xDeg, this.props.yDeg],
    });

    return (
      <View style={styles.container}>
        <Animated.View //returns Animated.View
          style={{
            marginLeft,
          }}>
          <Image style={{width: 100, height: 100}} source={{uri: IMG_URI}} />
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 150,
  },
});
export default AnimationLeftView;
